FILES=dynarray.tex jfla-talk.tex

LATEXMK=latexmk -pdf -bibtex
.PHONY: mk
mk:
	$(LATEXMK) $(FILES)

clean:
	latexmk -c
	rm -f *.{log,bbl,nav,rev,snm,vrb,vtc}

.PHONY: all clean
