Merci pour ces rapports ! Nous allons essayer d'utiliser vos remarques pour améliorer la présentation. En attendant, voici des réponses aux deux questions principales qui nous ont été posées:
- "quels contextes considérons-nous pour la sûreté" (rapport B)
- "peut-on éviter le boxing en abandonnant les accès non-sûrs ?" (rapport C)


## B:  Quels contextes considérons-nous pour la sûreté ?

> Un point technique me travaille particulièrement, je ne suis pas complètement sûr qu'il soit bien fondé. Je le livre aux auteurs, et leurs laisse juge de l'adresser dans le papier, la présentation, ou l'ignorer selon qu'ils le jugent pertinent ou non.
> - La sûreté mémoire doit être assurées, même contre un usage incorrect de notre structure. Mais quels sont ces contextes incorrectes que l'on considère exactement ? Certes ils peuvent être concurrents, mais sont-ils syntaxiquement bien typés ? Sémantiquement bien typés ? Quelconques ?

Le choix de vocabulaire pour répondre n'est pas évident: est-ce qu'un usage de Obj.magic ou Array.unsafe_get accepté par le typeur OCaml est "syntaxiquement bien typé" ? Dans ce contexte on a envie de dire "non".

Du plus restrictif au plus large, on pourrait parler des contextes suivants:

- les contextes qui sont dans le fragment sûr de OCaml, et syntaxiquement bien typés (facile à définir précisément)

- les contextes qui sont "sémantiquement sûrs et bien typés", mais où la notion de type "sémantique" n'empêche pas l'utilisation concurrente mal synchronisée du dynarray (moins clair à définir précisément)

- les contextes qui sont sûrs contre toutes les implémentations entièrement sûres de Dynarray (facile à définir précisément).

> En particulier, comme pointé dans une remarque page 15, votre spécification faible, contrairement à la forte, regarde le typage des objets stockés dans la structure. Cela suggère t il que l'on n'a pas d'obligation de memory safety si l'environnement utilise obj.magic pour stocker des éléments mal typés dans notre tableau ?

Le type τ des éléments dans la spécification est un type sémantique (iType), donc on a plus de flexibilité qu'avec un type OCaml. Par exemple on pourrait choisir le type des "fonctions définies sur [0; 10] et croissantes", et avoir un programme qui est non-sûr si cet invariant n'est pas respecté.

On pourrait imaginer un programme qui utilise le tableau de façon hétérogène, par exemple en stockant des entiers dans les cases paires et des booléens dans les cases impaires. Notre formalisation actuelle ne permet pas d'exprimer cela, mais nous pensons que ce serait facile à exprimer en rendant les τ dépendants de l'indice dans le tableau.

> Et peut-être ce qui m'embête le plus : mon Iris-foo est un peu trop léger en ce moment, mais sauf erreur il n'est pas possible de refléter ces questions au niveau de heaplang : cela serait-il possible au niveau de Melocoton ? Faut-il une sémantique opérationnelle de  Obj.Magic pour en parler ?

Heaplang est non-typé et permet l'arithmétique de pointeurs, donc on peut exprimer les programmes non-sûrs (et buggués) qu'on veut. On est limité pour casser l'abstraction sur les types de données, il n'est pas possible de voir les entiers comme des champs de bits ou les sommes natives comme des blocs taggés. Mais on peut définir ses propres "sommes bas-niveau" (sous forme de blocs) ou "champs de bits" si on veut vraiment faire tout en bas niveau.


## C: Déboîter en vérifiant plus de bornes ?

> Ceci dit, après avoir lu l'article, j'ai été pris d'un doute profond.
> (1) on m'explique qu'il faut faire du unsafe_get pour gagner en perf,
> (2) on m'explique pour faire du unsafe_get sans segfault il faut faire qqchose,
> (3) on m'explique que parmi les quelque-choses possibles, le moins pire
>     est de boxer les valeurs du tableau,
> (4) on m'explique que certes le boxing est coûteux, mais que sur 3 benchmarks
>     ce n'est pas si dramatique que ça.
> 
> Et là, je me dis : "mais attends, si ça se trouve, la solution finale est bien plus lente que de remplacer unsafe_get par get dès le départ...".  Et je ne vois pas où le papier parle de cette comparaison.  Il faut faire cette comparaison, non seulement contre un Array.get naif, mais aussi contre un Array.get avec un bound-check compilé de manière un minimum astucieuse, comme j'explique ci-dessous.

Il nous semble qu'il n'est pas possible d'éviter le boxing en rajoutant les bound-checks. Les bound-checks garantissent qu'on n'accède pas en dehors du tableau support; le boxing sert à traiter proprement les cases "vides" qui sont dans le tableau support, dans la partie d'espace au-delà de la taille dynamique. Si on élimine le boxing il faut choisir des valeurs pour ces cases vides au type 'a, et il n'y a pas de choix naturel, sûr, qui préserve la vivacité mémoire minimale.

En prenant un peu de distance: il est certainement possible d'écrire une implémentation aux performances tout à fait acceptables sans utiliser unsafe_{get,set} -- l'ordre de grandeur de surcoût est comparable ou inférieur à celui du boxing, que nous jugeons acceptable. Le point important à notre avis n'est pas de garder les accès non-sûrs à tout prix, mais de bien prévenir les personnes qui ont déjà du code non-sûr écrit pour OCaml 4 qu'il n'est plus forcément correct pour OCaml 5. Si jamais elles souhaitent garder certains accès non-sûr, nous expliquons précisément comment relire et raisonner sur le code.
