+ Introduction
  + problème de sûreté de Dynarray dans OCaml 5
  + résumé de l'histoire de la PR
  + formulation Iris
  + résumé du travail de vérification réalisé
+ Dynarray dans OCaml 5
  + Rappeler l'API de Dynarray et les usages
  + parties faites dans l'introduction, à ne pas remettre ici
    + Version séquentielle: Array.unsafe_{get,set}
    + Problème de sûreté dans OCaml 5
      + rappel sur le modèle mémoire OCaml 5
      + besoin: correct en séquentiel, sûr en concurrent
    + Version finale (sûre)
  + Benchmarks
  + Cases vides.
    + mentionner l'approche Obj.magic séquentielle (non sûre)
    + 'a option array => 'a elem array
    + pistes non-sûres envisagées (rapide)
    + benchmarks (rapide)
  + Invalidation des itérateurs
    + plusieurs choix possibles
    + choix final: échouer le plus souvent possible
    + future work: versions atomiques
  + Histoire de la PR OCaml 5
+ Raisonner sur Dynarray
  + processus de review
    + deux passes: séquentiel puis concurrent
    + sûreté mémoire: invariants faibles, raisonnement local
  + "correct en séquentiel, sûr en concurrent"
    (formulation d'Armaël)
    qu'est-ce que ça veut dire, formellement ?
    => on va faire en Iris
    + vérification de la correction en logique de séparation
    + vérification de la sûreté en présence d'opérations "unsafe":
      types sémantiques (pour HeapLang)
  + on montre la version finale pour une fonction ('add_last')
    + modèle: expliquer la possession unique
      (séquentiel ou 'concurrent bien synchronisé')
    + invariants faibles (juste les invariants, pas toute la mécanique d'Iris)
  + ce qu'on a fait
    + implémentation d'un noyau de Dynarray en HeapLang
    + on montre les paires de specs des différentes fonctions
    + zoom sur la vérification d'une ou deux implémentations: get, add/push
    + construction en couches: Chunk, Array, Dynarray
  + zoom technique: itérations atomiques
+ Discussion et conclusion
  + travail en cours: version Cosmo
  + travail futur:
    + exceptions
    + invalidation des itérateurs
    + automatiser vérification sûreté (Gospel ?)
  + Related Work
    + RustHornBelt: A Semantic Foundation for Functional Verification of Rust Programs with Unsafe Code
    + RefinedRust (https://people.mpi-sws.org/~gaeher/slides/refinedrust_rw23.pdf)
    + Specifying and Verifying Higher-order Rust Iterators

Explications pour Iris: les fournir au fil de l'eau, plutôt qu'en un
indigeste paragraphe introductif. Quand on utilise une nouvelle
construction on fournit une explication informelle pour les
non-spécialistes.
