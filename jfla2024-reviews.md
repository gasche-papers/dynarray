JFLA 2024 Paper #29 Reviews and Comments
===========================================================================
Paper #29 Correct tout seul, sûr à plusieurs.


Review #29A
===========================================================================

Avis général
------------
A. Très positif

Expertise du relecteur
----------------------
Y. Informé

Résumé de l'article et contributions
------------------------------------
OCaml a basculé depuis sa version 5 sur un modèle mémoire concurrent.
Un des choix (et une des difficultés) a été de continuer de garantir la sûreté mémoire grâce au typage, même en présence de courses critiques.

Les auteurs considèrent ici le problème de certaines fonctions implémentées à l'aide de primitive non sûres (Array.unsafe_set/get), qui étaient sûres dans un contexte séquentiel mais ne le sont plus dans un contexte concurrent.

L'exemple typique que considèrent et traitent les auteurs est celui des tableaux redimensionables (le module Dynarray).
En effet, ce module est implémenté à l'aide des fonctions Array.unsafe_set/get afin d'éviter des tests d'indices superflus... qui ne le sont plus en présence de concurrence.
(Par ex, car un thread concurrent peut redimensionner le tableau entre le moment où l'on a vérifié des choses et le moment où l'on y accède).
NB: les auteurs ne s'intéressent pas à la correction fonctionnelle dans le cas concurrent, seulement à l'efficacité et à la sûreté mémoire

Les deux contributions principales sont les suivantes:
- le développement d'une nouvelle version du module Dynarray, efficace et sûr pour OCaml 5
- la formalisation en Coq+Iris du coeur de cette nouvelle version, d'une preuve de sa correction dans le cas séquentiel, et d'une preuve de sa sûreté dans le cas concurrent.

Le papier est également structuré de manière à apporter deux contributions annexes:
- la discussion détaillée d'un certain nombre de choix qui ont été faits, et d'alternatives
- la documentation du long processus qui a mené à l'approbation de cette nouvelle version pour la librairie standard d'OCaml

Résumé d'évaluation
-------------------
L'ensemble des contributions forme un tout remarquable.
L'article est original par son style et la variété de ses contenus. 
Le problème qui y est étudié peut sembler facile de premier abord, mais on comprend en lisant l'article qu'il n'en est rien: l'espace des possibilités est très grand, et les contraintes (justifiées!) d'ingénérie logicielle sont très fortes.

Outre la sûreté mémoire dans le cas concurrent, les auteurs discutent en détail deux points importants:
- la vivacité: à tout instant, le tableau utilisé en interne ne doit contenir que des références vers des objets que le tableau "utilisateur" est censé contenir, afin de permettre au gc de faire son travail.
- la sémantique des itérateurs dans le cas concurrent: que doit-il se passer lorsqu'un tableau est modifié pendant qu'il est parcouru?
Dans les cas, de nombreux choix sont possibles, que les auteurs détaillent tout en résumant de longues discussions entre développeurs et utilisateurs experts d'OCaml.

Il me semble tout à fait intéressant que cet aspect du travail soit documenté, et il me semble que les JFLAs sont un très bon lieu pour celà.
Notamment, cet article peut aider à de jeunes chercheur·e·s ou ingénieur·e·s à mieux comprendre (et accepter?) certaines phases du dévelopement logiciel.

Évaluation détaillée, commentaires aux auteurs
----------------------------------------------
Le style est agréable (à mon goût) mais souvent à la limite du trop familier.
Attention pour de prochaines soumissions, c'est à double tranchant, l'effet produit pouvant dépendre fortement du (re)lecteur.

## minor

abstract: "des changements à la manière d'écrire" -> ".. dans la manière d'écrire"

p3 "bibliothèque standard de 5.2" -> .. d'OCaml 5.2

p6 "l'implémentation de Simon" -> .. de Simon Cruanes (possiblement à d'autres endroits aussi, l'usage des prénoms seuls est trop familier)

p6 footnote 2 "les nombreux flottants"

p7 "indépendente" -> ..a..

p10 "bibliothèque standard minimal*e*"

p12 "indices compris * les bornes"



Review #29B
===========================================================================

Avis général
------------
B. Positif

Expertise du relecteur
----------------------
X. Expert

Résumé de l'article et contributions
------------------------------------
Cet article dépeint essentiellement l'histoire du portage de la structure de
tableaux dynamiques de OCaml 4 à OCaml 5. Ce faisant, il couvre plusieurs
aspects techniques intéressants.
1. Il présente un problème générique
relativement contre-intuitif : les promesses de sécurité mémoire de OCaml
obligent à prendre en compte de nouveaux contextes d'utilisation en OCaml 5, même si
ceux-ci sont interdits par le contrat de la structure, amenant des structures
séquentielles à devenir invalides---les tableaux dynamiques en sont un exemple.
2. Il décrit le cas d'étude en détail, et discute des choix de design associés.
3. Il s'attarde sur des aspects de sociaux-ingénierie en décrivant l'histoire
de la PR ayant finalement été acceptée dans la librairie standard.
4. Finalement, les auteurs présentent ce qui est peut-être plus proche d'une contribution
de recherche traditionnelle : ils proposent une méthodologie pour formaliser la dualité de
contrats nécessaires à établir: correction fonctionnelle dans un cas fortement synchronisé
(Correct tout seul), et la sécurité mémoire dans le cas général (sûr à plusieurs). Ils utilisent
pour deux spécifications indépendantes formalisées dans Iris.

La partie pratique est déjà un succès, ayant conduit à une pull request acceptée. La partie formelle est encore relativement préliminaire, ayant un certain nombre d'extensions et avenues d'amélioration identifiées---notamment vis à vis du modèle mémoire considéré.

Résumé d'évaluation
-------------------
J'ai trouvé la lecture de cet article très enrichissante.
Je n'avais jamais pensé au problème général au centre du papier : celui-ci est
intéressant et bon à avoir à l'esprit. Les discussions sur les choix de design
et l'interaction avec la communauté pour atteindre un PR sont par moment un peu
longues, et parfois légèrement trop teintées de privates jokes et défouloir pour
les auteurs. Mais en même temps, elles apportent un vaste lot d'informations très
intéressantes, et pour certaines d'entre elles peu ou prou impossibles à trouver dans un papier traditionnel.
Enfin, j'aurais aimé que la partie 3 soit (beaucoup) plus développée, mais elle introduit
déjà des contributions riches, notamment l'observation du parallèle entre le problème traité par RustBelt,
et celui au centre de cet article.

En résumé, même si j'aurais aimé que l'article soit légèrement plus poli, et
qu'il offre un focus un petit peu plus resserré sur les aspects techniques de la partie 3,
je pense qu'il a sa place au JFLA, et en particulier suis confiant qu'il donnera lieu à des
échanges riches durant la conférence.

Évaluation détaillée, commentaires aux auteurs
----------------------------------------------

Un point technique me travaille particulièrement, je ne suis pas complètement sûr qu'il soit bien fondé. Je le livre aux auteurs, et leurs laisse juge de l'adresser dans le papier, la présentation, ou l'ignorer selon qu'ils le jugent pertinent ou non.
- La sûreté mémoire doit être assurées, même contre un usage incorrect de notre structure. Mais quels sont ces contextes incorrectes que l'on considère exactement ? Certes ils peuvent être concurrents, mais sont-ils syntaxiquement bien typés ? Sémantiquement bien typés ? Quelconques ? En particulier, comme pointé dans une remarque page 15, votre spécification faible, contrairement à la forte, regarde le typage des objets stockés dans la structure. Cela suggère t il que l'on n'a pas d'obligation de memory safety si l'environnement utilise obj.magic pour stocker des éléments mal typés dans notre tableau ? Et peut-être ce qui m'embête le plus : mon Iris-foo est un peu trop léger en ce moment, mais sauf erreur il n'est pas possible de refléter ces questions au niveau de heaplang : cela serait-il possible au niveau de Melocoton ? Faut-il une sémantique opérationnelle de  Obj.Magic pour en parler ?

Remarques mineures :

- Page 1 : Il est difficile de comprendre à ce stade ce qu'est la taille «n» précisément. En particulier, on apprend que length(arr) >= n, mais pas le lien entre length/position et n.
- page 2 : «C'est rageant» -> Je ne suis pas opposé au ton assez familier du papier, mais je trouve ce mot vraiment hideux. Puis-je négocier une substitution pour «exaspérant» ?
- page 5 : «pour limiter de conserver» -> grammaire
- page 5, section 2.4.1 : Je ne comprends pas cette option. Clairement, avant même de discuter de complexité de "set", utiliser une des valeurs du tableau n'a aucun sens puisque
    l'on ne pourra décider si le tableau contient des occurrences multiples de cette valeur, ou si elles indiquent le vide. Ou ai-je raté quelque chose ?
- page 6 : accès concurrents imprévu -> imprévus
- page 6 (footnote) : les "nombreux flottants». Les flottants n'étant précisément pas si nombreux que cela par nature, je suppose que vous vouliez écrire «nombres flottants».
- page 6 (footnote) : pique à Simon Cruanes mise à part, je ne comprends pas le lien entre l'utilisation de Obj.magic et les flottants. Pourriez-vous expliciter dans le texte la pertinence de la question ?
- page 8 : choix diff`rents -> différents
- page 8 : point manquant en fin de phrase
- page 8 : des comportement -> comportements
- page 8 : ont été rajouté -> rajoutés
- page 8 : qui est permet -> grammaire
- page 12: indices compris les bornes des tableaux -> grammaire
- page 14: consistent -> consistant (X2)
- page 14: Dynarray l'aide de triplets de Hoare -> grammaire
- page 17: intredit -> interdit



Review #29C
===========================================================================

Avis général
------------
A. Très positif

Expertise du relecteur
----------------------
X. Expert

Résumé de l'article et contributions
------------------------------------
L'utilisation de Array.unsafe_get/set est une pratique courante dans des
bibliothèques OCaml. Éviter la vérification des bornes améliore un peu
les performances. Mais, ces fonctions pose un problème fondamental, depuis
parallel-OCaml (Ocaml 5). En présence d'accès concurrents mal maîtrisés (races),
on peut se retrouver à faire des segfault. Panique à bord sur le bateau OCaml !

Les auteurs ont participé au développement d'une solution, qui non seulement
est implémentée et en partie prouvée correcte, mais si j'ai bien compris va
atterir dans la lib standard d'OCaml 5, ce qui est une vraie prouesse.

Résumé d'évaluation
-------------------
Le problème de l'apparition de segfault avec la concurrence était, il me
semble, relativement inattendu. Il me semble que l'existence du problème et
de ses solutions méritent d'être largement diffusés, non seulement au JFLA,
mais prochainement j'encourage les auteurs à soumettre à ICFP.

Ceci dit, après avoir lu l'article, j'ai été pris d'un doute profond.
(1) on m'explique qu'il faut faire du unsafe_get pour gagner en perf,
(2) on m'explique pour faire du unsafe_get sans segfault il faut faire qqchose,
(3) on m'explique que parmi les quelque-choses possibles, le moins pire
    est de boxer les valeurs du tableau,
(4) on m'explique que certes le boxing est coûteux, mais que sur 3 benchmarks
    ce n'est pas si dramatique que ça.

Et là, je me dis : "mais attends, si ça se trouve, la solution finale est bien
plus lente que de remplacer unsafe_get par get dès le départ...".
Et je ne vois pas où le papier parle de cette comparaison.
Il faut faire cette comparaison, non seulement contre un Array.get naif,
mais aussi contre un Array.get avec un bound-check compilé de manière
un minimum astucieuse, comme j'explique ci-dessous.

Mon intuition (et ce n'est pas que la mienne) est que la vérification de
bornes ne coûte pas très cher; et serait quasiment inobservable si elle est
bien implémentée par le compilateur. Il y a deux types de programmes, ceux qui
font des accès occasionnels à un tableau, et ceux qui font des accès hyper intensifs.

Pour ceux qui font des accès occasionnels, l'accès au header et l'accès à la case 'i'
peuvent faire tous les deux des cache-misses coûteux. Lorsque je vois l'implem
actuelle :

```
  CAMLprim value caml_array_get_addr(value array, value index)
  {
    intnat idx = Long_val(index);
    if (idx < 0 || idx >= Wosize_val(array)) caml_array_bound_error();
    return Field(array, idx);
  }
```

Je me demande si on ne gagnerait pas à faire un prefetch explicite sur la case 'i'
(__builtin_prefetch a été ajouté à GCC 3.1 en 2002; je vois qu'OCaml utilise le
prefetching dans le major-GC depuis 2021).

```
  CAMLprim value caml_array_get_addr(value array, value index)
  {
    __builtin_prefetch (&Field(array, idx)); // n'échoue pas même si hors bornes
    intnat idx = Long_val(index);
    if (idx < 0 || idx >= Wosize_val(array)) caml_array_bound_error();
    return Field(array, idx);
  }
```

Ensuite, il y a les programmes intensifs en accès. Typiquement, une boucle sur
un tableau. Par exemple :

```
  for i = 0 to n-1 do
    x := !x + t.(i);
  done
```

Pour ceux là, il faut dérouler la boucle pour pouvoir factoriser les checks.
Par exemple si on déroule 4 fois.
```
  for i = 0 to 4*(n-1)/4 stepby 4 do
    x := !x + unsafe_get t i;
    x := !x + unsafe_get t (i+1);
    x := !x + unsafe_get t (i+2);
    x := !x + unsafe_get t (i+3);
  done;
  ...et postlude pour gérer le reste de la division par 4
```

Ainsi formulé, le check peut être factorisé, et calculé une fois sur 4.

```
  for i = 0 to 4*(n-1)/4 stepby 4 do
    if i >= 0 && i+3 < n then begin
      x := !x + read (t+i);
      x := !x + read (t+i+1);
      x := !x + read (t+i+2);
      x := !x + read (t+i+3);
    else begin
      x := !x + safe_get t i;
      x := !x + safe_get t (i+1);
      x := !x + safe_get t (i+2);
      x := !x + safe_get t (i+3);
    end
  done;
  ...et postlude pour gérer le reste de la division par 4
```

Encore mieux, on peut souvent voir par analyse statique triviale que
la conditionnelle est toujours vraie, de part la valeur de 'i' entre
les bornes de la boucle for. C'est tout bête, mais ça permet d'enlever
complètement le bound check dans ~95% des cas sur les boucles intensives.

Au passage, dérouler la boucle permet de factoriser d'autres éléments du code.
Il permet aussi d'autres optimisations critiques. (Ainsi que,
un jour lointain, l'utilisation d'opérations vectorielles.)

Modifier le compilateur, c'est peut être trop demander à court terme.
Par contre, vous pourriez vraisemblablement benchmarker du code :
(1) Vector-OCAML5 "unsafe_get avec boxing" vs "safe-get sans boxing".
(2) avec la version de array_get avec prefetch, et
(3) avec une boucle déroulée à la main et la vérif de bornes factorisée pour 8 itérations.

Évaluation détaillée, commentaires aux auteurs
----------------------------------------------
p2: on parle de with_capacity, qui semble résoudre le problème,
mais le reste du papier ne reparle jamais de cette fonction,
c'est bizarre.

p8: je ne suis pas tout à fait d'accord qu'il n'y a pas de cas d'usage pour
itération avec modification. J'ai déjà écrit dans ma vie :
```
Array.iteri (fun i v -> t.(i) <- v + 1) t
et
Array.iteri (fun i v -> t.(i) <- v + u.(i)) t
```
Ceci dit, une possibilité c'est d'introduire `array_mapi_in_place` dans la lib,
pour pouvoir faire:
```
array_mapi_in_place (fun i v -> v+1) t
array_mapi_in_place (fun i v -> v+u.(i)) t
```

p8: il y a des ambiguités dans les phrases "échoue si elle observe une modif"
et "efforts raisonnables pour détecter ces cas et échouer". Il faut expliciter :
dans certains cas, le code lèvera une exception; dans d'autres, il n'y aura pas
d'exception, mais des valeurs arbitraires (du bon type) seront lues.

p10: on ne peut pas écrire "map est 78x plus lent que array" sans préciser
le nombre d'élément dans la séquence, car la complexité asymptotique n'est pas la même.
En fait, même pour comparer Hashtbl et Array, le speedups va aussi varier
selon la taille de la séquence, en raison des effets du cache.

p12: "usage concurrent non synchronisé" : merci de toujours préciser que
les races read-read sont ok, c'est read-resize et write-resize qui posent
problème (en incluant pop/push/fit dans resize). Quid de read-write?

p17: le "bouh!" est superflu je pense, même pour un papier jfla.
-- lorsqu'OCaml aura l'impact de Python sur le monde, on pourra en reparler :)

p17 "C++ casse la sûreté mémoire" : je ne savais pas qu'il y avait de la
sûreté mémoire en C/C++. à préciser ou supprimer.

p18: RefinedRust: on reste sur sa faim, est-ce que les problématiques sont
exactement les mêmes ? à peu près les mêmes ? quelles différences avec
ce qui se passe en OCaml ?


Typos:
- bravo pour la meilleur typo du monde : "Et pour les nombreux flottants ?"
  bien cachée dans la footnote 2.
- 2.4.3 : imprévu+s+
- d’utilier
- choix diff´rents
- p11: "échoue, est considérée" : manque un "et" ?
- mars 2023... manque un point à la fin.
