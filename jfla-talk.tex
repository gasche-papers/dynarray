\RequirePackage[dvipsnames]{xcolor}

\documentclass[usenames,dvipsnames,table]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}

\usepackage{reversion}
\usepackage{mylistings-slide}
\usepackage{mybiblio}
\usepackage{mymath}
\usepackage{myhyperref}
\usepackage{mybeamer}
\usepackage{mycomments}

\usepackage{tikz}
\usetikzlibrary{
	decorations.pathreplacing,
	positioning
}

\usepackage{mymacros}

\newcommand{\includepicture}[1]{\includegraphics[height=3cm]{pictures/#1}}

\hypersetup{breaklinks=true,colorlinks=true,citecolor=purple}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{\hfill\insertframenumber\hfill\vspace{3mm}}

\title{Correct tout seul, sûr à plusieurs}
\date{\today}
\author{Clément Allain, Gabriel Scherer} % ordre alphabétique
\institute{INRIA (Cambium), $\quad$ INRIA (Picube)}

%--------------------------------------------------
%--------------------------------------------------

\begin{document}

%--------------------------------------------------

\begin{frame}
  \titlepage

\vfill

\begin{center}
  \includepicture{Clement_Allain.jpg}
  \hspace{2em}
  \includepicture{Gabriel_Scherer.jpg}
\end{center}
\end{frame}

%--------------------------------------------------

\begin{frame}{This talk}
  The story of how we got \texttt{Dynarray} in the OCaml standard library.

  \vfill\pause

  ... and the horrors that lie beneath ...

  \vfill\pause

  with Coq proofs!
\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]{Dynarray: what?}
An array...
\begin{lstlisting}
val init : int -> (int -> 'a) -> 'a t

val get : 'a t -> int -> 'a
val set : 'a t -> int -> 'a -> unit

val length : 'a t -> int
\end{lstlisting}

\vfill\pause

that is also a stack (Daniel Bünzli):
\begin{lstlisting}
val create : unit -> 'a t
val add_last : 'a t -> 'a -> unit
val pop_last_opt : 'a t -> 'a option
\end{lstlisting}
\end{frame}

%--------------------------------------------------

\begin{frame}{Dynarray: why?}
  \begin{itemize}
  \item You want build an array by accumulating elements, \\
    but you don't know the size in advance.

    (Note: \texttt{Array.of\_list} may also work very well.)

  \item You want a stack or bag,
    but also indices and random access.
  \end{itemize}

  \vfill

  Classic examples:
  \begin{itemize}
  \item Priority queues stored in an array (textbook algorithm). \\
     Stdlib priority queues (@backtracking, January 2024) \\
    \url{https://github.com/ocaml/ocaml/pull/12871}
  \item The journal of a journalled data structure.
  \item The trail of a SAT/SMT solver.
  \item Clause sets in an automated prover.
  \end{itemize}
\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]{Dynarray: how?}
Implementation (\texttt{'a slot} is a secret for now):

\begin{lstlisting}
type 'a t = {
  mutable data : 'a slot array;
  mutable len : int;
}
\end{lstlisting}

\vfill

\input{dynarray-picture.tex}

\vfill

Capacity (backing array length). Space control (Simon Cruanes):
\begin{lstlisting}
val capacity : 'a t -> int
val ensure_capacity : 'a t -> int -> unit
val fit_capacity : 'a t -> unit
\end{lstlisting}
\end{frame}

%--------------------------------------------------

\begin{frame}{Story time (1)}
  Once upon a time, a brave, \emph{brave} contributor

  \vfill\pause

  \begin{center}
    \includepicture{Simon_Cruanes.jpg}
    (Simon Cruanes)
  \end{center}

  \vfill\pause

  wanted to improve the OCaml standard library

  by adding a \texttt{Dynarray} module from his \href{https://github.com/c-cube/ocaml-containers/}{containers} library.

  \vfill

  Many had tried before him...

  \begin{small}
    \begin{itemize}
    \item ...
    \item \url{https://discuss.ocaml.org/t/adding-dynamic-arrays-vectors-to-stdlib/4697/38}
    \item \url{https://github.com/ocaml/ocaml/pull/9122}
    \end{itemize}
  \end{small}
\end{frame}

%--------------------------------------------------

\begin{frame}{Story time}
  He held a secret meeting with two gate keepers of the stdlib

  \vfill\pause

  \begin{center}
    \includepicture{Florian_Angeletti.png}
    \hspace{3em}
    \includepicture{Gabriel_Scherer.jpg}
  \end{center}

  \vfill

  They brainstormed an API, and a PR was born.

  \vfill

  ``add `Dynarray` to the stdlib'' (@c-cube, September 2022) \\
  \url{https://github.com/ocaml/ocaml/pull/11563}
\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]{Horror 1: empty value}

\input{dynarray-picture.tex}

\vfill

What value should we store in the empty space?

\vfill

A user-provided default value: inconvenient API.

\vfill

The last user-provided value: space leak.

\vfill

\texttt{Obj.magic ()}: ew.

\vfill

\texttt{None}: ew. \hfill (\texttt{'a option array})
\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]{Horror 2: concurrency}
\input{dynarray-picture.tex}

\begin{lstlisting}
let[@inline] get v i =
  if i < 0 || i >= v.len then
    invalid_arg "CCVector.get";
  Array.unsafe_get v.data i
\end{lstlisting}

\vfill

What if another domain races on \texttt{v.len}?

\vfill

\texttt{unsafe\_get}: segfault (out of backing array)

\vfill

\texttt{Obj.magic ()}: segfault (out of user space)
\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]{Story, continued}
  After endless nights fighting the zombie hordes of \texttt{Obj.magic ()}, \\ the PR went into an eternal sleep.

  \vfill\pause

  Until:

  ``Dynarrays, boxed'' (@gasche, January 2023) \\
  \url{https://github.com/ocaml/ocaml/pull/11882}

  \vfill

  \begin{lstlisting}
    type 'a slot =
    | Empty
    | Elem of { mutable v : 'a }
  \end{lstlisting}

  \vfill

  Reassuring benchmarks.

  Problem solved?
\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]{Horror 3: iterator invalidation}
\begin{lstlisting}
val iter : ('a -> unit) -> 'a t -> unit
\end{lstlisting}

What happens if elements are added or removed during \texttt{iter}?

\vfill

\begin{enumerate}
\item something reasonable (but slower)?
\item weak memory model?
\item invalid, maybe an error?
\item invalid, always an error?
\end{enumerate}

\vfill

  \begin{center}
    \includepicture{Simon_Cruanes.jpg}
    \hspace{3em}
    \includepicture{Guillaume_Munch-Maccagnoni.jpg}
  \end{center}
\end{frame}

%--------------------------------------------------

\begin{frame}{Story, end}
  Many more months of feedback, changes, decisions.

  \vfill

  \begin{center}
  \includepicture{Daniel_Bunzli.png}
  \end{center}

  \vfill

  Clément Allain reviewed the code for correctness.

  \vfill

  \begin{center}
  \includepicture{Clement_Allain.jpg}
  \end{center}

  \vfill

  Merged in OCaml 5.2!
  (to be released soon)
\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]{Take away}

\begin{lstlisting}
let[@inline] get v i =
  if i < 0 || i >= v.len then
    invalid_arg "CCVector.get";
  Array.unsafe_get v.data i
\end{lstlisting}

\vfill

Public announcements:

\begin{center}
Library code must remain memory-safe for \emph{all} uses,

\emph{including} incorrect concurrent code.
\end{center}

\vfill\pause

\begin{center}
Consequence:

Some unsafe code that was \textcolor{blue}{\emph{perfectly fine}} with OCaml 4

is now \textcolor{red}{\emph{unsound}} with OCaml 5
\end{center}

\vfill

Time to review all your \text{unsafe\_\{get,set\}} calls.

\vfill

How do we \emph{reason} about this?
\end{frame}

%--------------------------------------------------

\begin{frame}
    
    \frametitle{Strong invariant for functional correctness}
    
    \centering
    
    \begin{tikzpicture}[yscale=-1]
        \draw (1,0) grid ++(2,1) ;
        \draw (4,0) grid ++(6,1) ;
        \draw (4,2) rectangle ++(1,1) ;
        \draw (6,2) rectangle ++(1,1) ;
    
        \node [align=center] (t) at (0,0.5) {$t$} ;
        \node [align=center] at (1.5,0.5) {$len$} ;
        \node [align=center] at (2.5,0.5) {$data$} ;
        \node [align=center] at (4.5,0.5) {Elem} ;
        \node [align=center] at (5.5,0.5) {$\cdots$} ;
        \node [align=center] at (6.5,0.5) {Elem} ;
        \node [align=center] at (7.5,0.5) {Emp} ;
        \node [align=center] at (8.5,0.5) {$\cdots$} ;
        \node [align=center] at (9.5,0.5) {Emp} ;
        \node [align=center] at (4.5,2.5) {$v_0$} ;
        \node [align=center] at (6.5,2.5) {$v_{len-1}$} ;
        
        \draw [thick, ->] (t) -- ++(1,0) ;
        \draw [thick, ->] (3,0.5) -- ++(1,0) ;
        \draw [thick, ->] (4.5,1) -- ++(0,1) ;
        \draw [thick, ->] (6.5,1) -- ++(0,1) ; 
    \end{tikzpicture}

\end{frame}

%--------------------------------------------------

\begin{frame}
    
    \frametitle{Weak invariant for memory safety}
    
    $t$ : $\tau$ t
    
    \bigskip
    
    \begin{center}
        \begin{tikzpicture}[yscale=-1]
            \draw (1,0) grid ++(2,1) ;
            \draw (4,0) grid ++(3,1) ;
        
            \node [align=center] (t) at (0,0.5) {$t$} ;
            \node [align=center] at (1.5,0.5) {$len$} ;
            \node [align=center] at (2.5,0.5) {$data$} ;
            \node [align=center] at (4.5,0.5) {$\tau$ slot} ;
            \node [align=center] at (5.5,0.5) {$\cdots$} ;
            \node [align=center] at (6.5,0.5) {$\tau$ slot} ;
            
            \draw [thick, ->] (t) -- ++(1,0) ;
            \draw [thick, ->] (3,0.5) -- ++(1,0) ;
        \end{tikzpicture}
        
        \[0 \leq len\]
    \end{center}
    
    \vfill
    \hrule
    \vfill
    
    $slot$ : $\tau$ slot
    
    \bigskip
    
    \begin{center}
        \begin{tabular}{c@{\hskip 0.2in}|@{\hskip 0.2in}c}
                $slot$ = Emp
            &
                \begin{tikzpicture}[yscale=-1]
                    \draw (1,0) rectangle ++(1,1) ;
                    
                    \node [align=center] (slot) at (0,0.5) {$slot$} ;
                    \node [align=center] at (1.5,0.5) {$\tau$} ;
                    
                    \draw [thick, ->] (slot) -- ++(1,0) ;
                \end{tikzpicture}
        \end{tabular}
    \end{center}

\end{frame}

%--------------------------------------------------

\begin{frame}
    
    \frametitle{A method\footnote{Thanks to Armaël Guéneau for stating clearly the dichotomy.} to reason about sequential/concurrent algorithms using unsafe features in \OCaml~5}
    
    \centering
    \large
    
    \begin{tabular}{p{0.4\textwidth}@{\hskip 0.2in}|@{\hskip 0.2in}p{0.4\textwidth}}
            \bf{Functional correctness} &
            \bf{Memory safety}
        \\\\
            Each function respects its specification. &
            Each function inhabits its semantic type.
        \\\\
            Strong invariant &
            Weak invariant
    \end{tabular}

\end{frame}

%--------------------------------------------------

\begin{frame}
    
    \frametitle{Reviewing \texttt{Dynarray}}
    
    \centering
    \LARGE
    
    \begin{tabular}{cl}
            &
            \texttt{Dynarray} review
        \\
            + &
            Separation logic (\Iris)
        \\
            + &
            \Coq
        \\\\
            = &
            \textsc{OCamlBelt}
        \\
            = &
            semantic typing implying memory safety
        \\\\
            $\approx$ &
            \textsc{RustBelt}
    \end{tabular}

\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]
    
    \frametitle{Formalization in the \Iris separation logic (mechanized in \Coq)}
    
    \centering
    
    \begin{lstlisting}
val create : unit -> 'a t
val make : int -> 'a -> 'a t
val init : int -> (int -> 'a) -> 'a t
val length : 'a t -> int
val get : 'a t -> int -> 'a
val set : 'a t -> int -> 'a -> unit
val add_last : 'a t -> 'a -> unit
val pop_last : 'a t -> 'a
val ensure_capacity : 'a t -> int -> unit
val ensure_extra_capacity : 'a t -> int -> unit
val fit_capacity : 'a t -> unit
val reset : 'a t -> unit
    \end{lstlisting}
    
\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]
    
    \frametitle{Strong invariant for functional correctness\dots in \Iris}
    
    \begin{lstlisting}
Definition dynarray_model /&t&/ /!vs!/ : iProp $\Sigma$ :=
  $\exists$ l /*data*/ /[slots]/ extra,
  $\ulcorner$/&t&/ = #/&l&/$\urcorner$ $\ast$
  /&l&/.[len] $\mapsto$ #(length /!vs!/) $\ast$
  /&l&/.[data] $\mapsto$ /*data*/ $\ast$
  array_model /*data*/ (/[slots]/ ++ replicate extra &&None) $\ast$
  [$\ast$ list] slot; v $\in$ /[slots]/; /!vs!/, slot_model slot v.
    \end{lstlisting}
    
    \vfill
    \hrule
    \vfill
    
    \begin{lstlisting}
Lemma dynarray_pop_last_spec /&t&/ /!vs!/ /*v*/ :
  {{{ dynarray_model /&t&/ (/!vs!/ ++ [/*v*/]) }}}
    dynarray_pop_last /&t&/
  {{{ RET /*v*/; dynarray_model /&t&/ /!vs!/ }}}.
\end{lstlisting}

\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]
    
    \frametitle{Weak invariant for memory safety\dots in \Iris}
    
    \begin{lstlisting}
Definition dynarray_type $\textcolor{teal}{\tau}$ `{iType _ $\textcolor{teal}{\tau}$} /&t&/ : iProp $\Sigma$ :=
  $\exists$ /&l&/,
  $\ulcorner$/&t&/ = #/&l&/$\urcorner$ $\ast$
  inv nroot (
    $\exists$ len cap /*data*/,
    $\ulcorner$0 <= len$\urcorner$ $\ast$
    /&l&/.[len] $\mapsto$ #len $\ast$
    /&l&/.[data] $\mapsto$ /*data*/ $\ast$
    array_type (slot_type $\textcolor{teal}{\tau}$) cap /*data*/
  ).
    \end{lstlisting}
    
    \vfill
    \hrule
    \vfill
    
    \begin{lstlisting}
Lemma dynarray_pop_last_type $\textcolor{teal}{\tau}$ /&t&/ :
  {{{ dynarray_type $\textcolor{teal}{\tau}$ /&t&/ }}}
    dynarray_pop_last /&t&/
  {{{ /*v*/, RET /*v*/; $\textcolor{teal}{\tau}$ /*v*/ }}}.
    \end{lstlisting}

\end{frame}
    
%--------------------------------------------------
    
\begin{frame}[fragile]
    
    \frametitle{\HeapLang (standard \Iris language)}
    
    \centering

    \begin{lstlisting}
Definition dynarray_pop_last : val :=
  $\lambda$: "t",
    let: "len" := dynarray_len "t" in
    let: "arr" := dynarray_data "t" in
    /&assume&/ ("len" <= array_length "arr") ;; 
    /&assume&/ (#0 < "len") ;;
    let: "last" := "len" - #1 in
    match: array_unsafe_get "arr" "last" with
    | None =>
        /&diverge&/ #()
    | Some "ref" =>
        array_unsafe_set "arr" "last" &None ;;
        dynarray_set_size "t" "last" ;;
        !"ref"
    end.
    \end{lstlisting}

\end{frame}

%--------------------------------------------------

\begin{frame}[fragile]
    
    \frametitle{What the mechanized \Iris proofs look like:}
    
    \scriptsize
    
    \begin{lstlisting}
Proof.
  iIntros "%$\Phi$ #Htype H$\Phi$".
  wp_rec.
  /!wp_apply!/ (dynarray_len_type with "Htype") as "%sz _".
  /!wp_smart_apply!/ (dynarray_data_type with "Htype") as "%cap %data #Hdata_type".
  /!wp_smart_apply!/ (array_size_type with "Hdata_type") as "_".
  /!wp_smart_apply!/ assume_spec' as "%Hcap".
  /!wp_smart_apply!/ assume_spec' as "%Hsz".
  /!wp_smart_apply!/ (/[array_unsafe_get_type]/ with "Hdata_type") as "%slot #Hslot".
  { /[lia.]/ }
  /!wp_apply!/ (opt_type_match with "Hslot"). iSplit.
  - /!wp_apply!/ diverge_spec.
  - iIntros "%r #Hr /=".
    /!wp_smart_apply!/ (/[array_unsafe_set_type]/ with "[Hdata_type]") as "_".
    { /[lia.]/ }
    { iSteps. }
    /!wp_smart_apply!/ (dynarray_set_size_type with "Htype") as "_".
    { lia. }
    /!wp_smart_apply!/ (reference_get_type with "Hr").
    iSteps.
Qed.
    \end{lstlisting}
    
\end{frame}

%--------------------------------------------------

%\begin{frame}
%    
%    \frametitle{Limitations \& future work}
%    
%    \Large
%    
%    \begin{itemize}
%        \item Exceptions.
%        \item Weak memory model (\Cosmo).
%        \item Automation ($\approx$ \textsc{RefinedRust}).
%        \item Verify other libraries using unsafe features.
%        \item \texttt{Dynarray} unboxed --- how to verify it?
%    \end{itemize}
%    
%\end{frame}

%--------------------------------------------------

\begin{frame}{End}
  \begin{center}
    Thanks!

    \vfill

    Questions?
  \end{center}
\end{frame}

%--------------------------------------------------

% Added after the JFLA talk, where we noticed that people wanted to
% discuss the OCaml implementation and asked us questions on the
% HeapLang version instead.

\begin{frame}[fragile]{Bonus slide: \texttt{pop\_last} in OCaml}
\begin{lstlisting}
let pop_last a =
  let {data = arr; len = length} = a in
  check_valid_length length arr;
  (* We know [length <= capacity a]. *)
  if length = 0 then raise Not_found;
  let last = length - 1 in
  (* We know [length > 0] so [last >= 0]. *)
  match Array.unsafe_get arr last with
  | Empty ->
      Error.missing_element ~i:last ~length
  | Elem s ->
      Array.unsafe_set arr last Empty;
      a.length <- last;
      s.v
\end{lstlisting}

\end{frame}

%--------------------------------------------------


\end{document}