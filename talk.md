# Correct tout seul, sûr à plusieurs

slot de 30mn
cible: 24 mn d'exposé + 6mn questions
    => 12 mn par personne

on parle en français

But:
- les gens ont une idée de notre travail
- s'ils veulent plus les détails, aller lire l'article


## Dynarray en OCaml

### Dynarray

noyau d'API pour expliquer
(inclure 'clear' pour exhiber ensuite les problèmes de concurrence)

impls existantes: CCVector, celle de Jean-Christophe, etc.

### Début de l'histoire

Simon+Florian -> Gabriel

### Problème 1: la concurrence

attention, problème par rapport à OCaml 4,
il faut écrire le code de façon plus prudente

### Problème 2: la valeur du vide

... => solution pas boxée

### Problème 3: invalidation des itérateurs

### Fin de l'histoire

Processus de relecture et prise de décision.

## Vérification

### Coucou, logique de séparation

rappel *très* rapide sur la logique de séparation / Iris

### Specs de base pour une structure impérative

possession unique => une forme de séquentialité

montrer les trucs familiers

### Problème de sûreté mémoire en OCaml 5

La relecture de l'implémentation a posé une question:

> Comment raisonner sur le problème de sûreté en preśence de
> concurrence, sur des invariants "faibles" ?

### Solution: types sémantiques


Remarque: même approche que RustBelt pour les "unsafe" de Rust, en
fait on est en train de parler des "unsafe" de OCaml -- les rares
constructions pour lequel le typage n'implique pas la sûreté mémoire.

### Demo: montrer vite fait une impl, des specs et leur vérification

L'implémentation HeapLang:
```
  Definition safe_dynarray_pop : val :=
    λ: "t",
      let: "sz" := safe_dynarray_size "t" in
      let: "data" := safe_dynarray_data "t" in
      assume ("sz" ≤ array_size "data") ;;
      assume (#0 < "sz") ;;
      let: "sz" := "sz" - #1 in
      match: array_unsafe_get "data" "sz" with
      | None =>
          diverge #()
      | Some "ref" =>
          array_unsafe_set "data" "sz" &None ;;
          safe_dynarray_set_size "t" "sz" ;;
          !"ref"
      end.
```
Vous voyez, on peut lire et comprendre un peu comme une version OCaml.

```
  Lemma safe_dynarray_pop_spec t vs v :
    {{{
      safe_dynarray_model t (vs ++ [v])
    }}}
      safe_dynarray_pop t
    {{{
      RET v;
      safe_dynarray_model t vs
    }}}.
```
Spec "séquentielle" (bien possédée), la traduire à haute voix.
(..._model t suppose la possession unique de t)

```
  Lemma safe_dynarray_pop_type τ t :
    {{{
      safe_dynarray_type τ t
    }}}
      safe_dynarray_pop t
    {{{ v,
      RET v;
      τ v
    }}}.
```
Spec "faible", la traduire à haute voix.
(..._type τ t est un invariant, pas unique)

[Gabriel]: j'ai rajouté τ explicitement, c'est un paramètre de section.


On montre la preuve *de loin*:
```
  Proof.
    iIntros "%Φ #Htype HΦ".
    wp_rec.
    wp_apply (safe_dynarray_size_type with "Htype"). iIntros "%sz _".
    wp_smart_apply (safe_dynarray_data_type with "Htype"). iIntros "%cap %data #Hdata_type".
    wp_smart_apply (array_size_type with "Hdata_type"). iIntros "_".
    wp_smart_apply assume_spec'. iIntros "%Hcap".
    wp_smart_apply assume_spec'. iIntros "%Hsz".
    wp_smart_apply (array_unsafe_get_type with "Hdata_type"); first lia. iIntros "%slot #Hslot".
    wp_apply (opt_type_match with "Hslot"). iSplit.
    - wp_apply wp_diverge.
    - iIntros "%r #Hr /=".
      wp_smart_apply (array_unsafe_set_type with "[$Hdata_type]"); [lia | iSmash |]. iIntros "_".
      wp_smart_apply (safe_dynarray_set_size_type with "Htype"); first lia. iIntros "_".
      wp_smart_apply (reference_get_type with "Hr").
      iSmash.
  Qed.
```
Éléments de langage:
- c'est pas trop gros et assez simple,
  on appelle une tactique par construction du programme
- l'action se passe pour les unsafe_{get,set}, il faut vérifier
  une condition sur l'indice, on appelle `lia` pour le faire
- on pourrait automatiser ça encore mieux

## Future work

[Gabriel]: prévoir qu'on n'aura pas le temps d'en parler

- Dynarray: implémentation unboxée
- Quelles autres bibliothèques sont touchées par ce problème de sûreté ?
- Cosmo / mémoire faible
- comptage de modifications
- ébaucher une version plus concurrente en ayant un get_opt

+ relecture pour intégration dans la bibliothèque standard
  + exemple : pop
  + accès non sûr surligné
  + commentaires
+ correction fonctionnelle
  + pas spécialement de difficulté
  + invariant fort
  + schéma : tableau support, plage valide
+ sûreté mémoire
  + montrer exécution plantée avec pop modifié et usage incorrect
  + typage fort = si programme typé, pas de plantage
  + style défensif = vérifications dynamiques
  + invariant faible
+ formalisation en Iris
  + montrer code
  + pointer différences
    + exceptions
    + mémoire faible
  + invariant fort
  + iType
  + invariant faible
  + array
+ continuations
  + mémoire faible
  + exceptions
  + automatisation (voir RefinedRust)

Although Dynarray is meant to be used in a sequential context, its implementation must ensure memory safety even in the case of improper use, e.g. concurrent updates.
OCaml~5 does ensure memory safety for well-typed programs but, for performance reasons, Dynarray uses unsafe features that require careful design.
To reason about its correctness, one can introduce strong invariants (sequential reasoning) and weak invariants (concurrent reasoning).
Following these guidelines, Clément Allain formalized and verified the core of Dynarray in the Iris separation logic~\cite{hal-01945446}.
